<?php 

class EUserSelect extends CWidget {
	public $users;
	public $valueElementId;
	public $multiselect = false;
	public $dynamic = false;
	
	public $allowNew = false;
	public $submitId;
	
	public function init() {
		//Options
		if ($this->multiselect)
			$this->multiselect = 'true';
		else
			$this->multiselect = 'false';
			
		if ($this->dynamic)
			$this->dynamic = 'true';
		else
			$this->dynamic = 'false';
		
		if ($this->allowNew)
			$this->allowNew = 'true';
		else
			$this->allowNew = 'false';
		
		$script = 'var EUserSelectElementId = "#'.$this->valueElementId.'";
		var EUserSelectMultiselect = '.$this->multiselect.';
		var EUserSelectDynamic = '.$this->dynamic.';
		var EUserSelectAllowNew = '.$this->allowNew.';
		var EUserSelectSubmitId = "#'.$this->submitId.'";';
		
		Yii::app()->clientScript->registerScript('EUserSelectOptions', $script, CClientScript::POS_HEAD);
		
		//Assets
		$jsFile = dirname(__FILE__).'/js/userselect.js';
		$cssFile = dirname(__FILE__).'/css/userselect.css'; 
		$js = Yii::app()->getAssetManager()->publish($jsFile);
		$css = Yii::app()->getAssetManager()->publish($cssFile);
		Yii::app()->clientScript->registerScriptFile($js, CClientScript::POS_END);
		Yii::app()->clientScript->registerCssFile($css);
		
		parent::init();
	}
	
	public function run() {
		$this->render('list', array(
			'users' => $this->users,
		));
		
		parent::run();
	}
}

?>