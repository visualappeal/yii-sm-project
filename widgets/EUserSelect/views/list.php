<?php if (is_array($users) and (count($users) > 0)): ?>
	<input type="text" id="userselect-filter" value="" placeholder="<?php echo Yii::t('EUserSelect', 'Filter'); ?>" />
	
	<ul id="userselect">
	<?php foreach ($users as $user): ?>
		<?php $this->render('_user', array(
			'user' => $user,
		)); ?>
	<?php endforeach; ?>
	</ul>
<?php else: ?>
	<span id="userselect-nousers"><?php echo Yii::t('EUserSelect', 'Es nehmen schon alle Mitarbeiter an dem Projekt teil.'); ?></span>
	<ul id="userselect"></ul>
<?php endif; ?>