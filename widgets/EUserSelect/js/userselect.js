jQuery.expr[':'].Contains = function(a,i,m){
    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
};

$(document).ready(function() {
	var userlistSize = $('#userselect > li').size();
	var userlist = new Array(userlistSize);
	
	var elementActive = false;
	
	var i = 0;
	$('#userselect > li').each(function() {
		userlist[i] = new Object();
		userlist[i]['id'] = $(this).data('userselect-id');
		userlist[i]['name'] = $(this).data('userselect-username');
	});

	$('#userselect > li').live('click', function() {
		if (!EUserSelectMultiselect) {
			if ($(this).hasClass('active')) {
				$(EUserSelectElementId).val('');
				$('#userselect > li').removeClass('active');
			}
			else {
				$('#userselect > li').removeClass('active');
				$(EUserSelectElementId).val($(this).data('userselect-id'));
				$(this).addClass('active');
			}
		}
		else {
			if ($(this).hasClass('active')) {
				$(this).removeClass('active');
			}
			else {
				$(this).addClass('active');
			}
			
			$(EUserSelectElementId).val('');
			var newValue = '';
			$('#userselect > li.active').each(function() {
				newValue = newValue + ',' + $(this).data('userselect-id');
			});
			
			$(EUserSelectElementId).val(newValue);
		}
		
	});
	
	if (EUserSelectAllowNew) {
		$(EUserSelectSubmitId).click(function() {
			if ($(EUserSelectElementId).val() == '') {
				$(EUserSelectElementId).val($('#userselect-filter').val());
			}
		});
	}
	
	$('#userselect-filter').change(function() {
		var filter = $(this).val();
		if (filter) {
			$('#userselect').find(".userselect-username:not(:Contains(" + filter + "))").parent().slideUp();
			$('#userselect').find(".userselect-username:Contains(" + filter + ")").parent().slideDown();
		} else {
			$('#userselect').find("li").slideDown();
		}
	}).keyup(function() {
		$(this).change();
	});
	
	if (EUserSelectDynamic) {
		$('#userselect-nousers').hide();
		$('#userselect').hide();
	}
});

//Refresh user list
function userSelectRefreshUser(html) {
	if (html.length < 1) {
		$('#userselect-nousers').show();
		$('#userselect').html('').hide();
	}
	else {
		$('#userselect-nousers').hide();
		$('#userselect').html(html).show();
	}
}