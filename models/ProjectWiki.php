<?php

/**
 * This is the model class for table "{{projects_wiki}}".
 *
 * The followings are the available columns in table '{{projects_wiki}}':
 * @property integer $id
 * @property integer $project_id
 */
class ProjectWiki extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProjectWiki the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project_wiki}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('project_id', 'required'),
			array('id, project_id', 'numerical', 'integerOnly'=>true),
			
			array('id, project_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'theProject' => array(self::BELONGS_TO, 'Project', 'project_id'),
			'pages' => array(self::HAS_MANY, 'ProjectWikiPage', 'wiki_id'),
		);
	}
	
	public function defaultScope() {
		return array(
			'with' => array(
				'pages',
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('project_id',$this->project_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}