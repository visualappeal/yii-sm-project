<?php

/**
 * This is the model class for table "{{projects}}".
 *
 * The followings are the available columns in table '{{projects}}':
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property integer $category_id 
 * @property integer $participants
 * @property integer $least_participants 
 * @property integer $max_participants
 */
class Project extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @access public
	 * @return Project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('title, user_id, category_id', 'required'),
			array('user_id, category_id, participants, least_participants, max_participants', 'numerical', 'integerOnly' => true),
			array('title, slug', 'length', 'max' => 70),

			array('id, title, slug, description, category_id, participants, least_participants, max_participants', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'theUser' => array(self::BELONGS_TO, 'User', 'user_id'),
			'theCategory' => array(self::BELONGS_TO, 'ProjectCategory', 'category_id'),
			'theWiki' => array(self::HAS_ONE, 'ProjectWiki', 'project_id'),
			'dates' => array(self::HAS_MANY, 'ProjectDate', 'project_id'),
			'staff' => array(self::HAS_MANY, 'ProjectStaff', 'project_id'),
			'images' => array(self::HAS_MANY, 'ProjectImage', 'project_id'),
		);
	}
	
	public function defaultScope() {
		return array(
			'with' => array(
				'theUser',
				'theCategory',
				'theWiki',
				'staff',
				'dates',
				'images',
			)
		);
	}
	
	public function scopes() {
		$t = $this->getTableAlias();
		return array(
			'index' => array(
				'limit' => 10,
				'with' => array(
					'dates' => array(
						'condition' => 'date_end >= NOW()',
					),
				)
			),
			'dashboard' => array(
				'limit' => 5,
				'with' => array(
					'dates' => array(
						'condition' => 'date_end >= NOW()',
					),
				)
			),
		);
	}
	
	public function behaviors() {
		return array(
			'sluggable' => array(
				'class' => 'application.modules.project.extensions.behaviors.SluggableBehavior.SluggableBehavior',
				'columns' => array('title'),
				'unique' => true,
				'update' => false,
			),
		);
	}
	
	/**
	 * Get all categories
	 */
	public function getCategories() {
		$categories = ProjectCategory::model()->findAll();
		
		return EBootstrap::listData($categories, 'id', 'title');
	}
	
	/**
	 * Get the HTML encoded description
	 */
	public function getDescriptionEncode() {
		return $this->description;
	}
	
	/**
	 * Get overview of participants
	 */
	public function getParticipantsOverview() {
		$html = '';
		if ($this->max_participants > 0) {
			if ($this->max_participants > $this->participants + 2)
				$label = 'success';
			elseif ($this->max_participants > $this->participants)
				$label = 'warning';
			else
				$label = 'important';
			
			$html .= EBootstrap::ilabel(Yii::t('Project', '{current}/{max}', array(
				'{current}' => $this->participants,
				'{max}' => $this->max_participants,
			)), $label)." ";
		}
		if ($this->least_participants > 0) {
			if ($this->least_participants > $this->participants)
				$label = 'warning';
			else
				$label = 'success';
			
			$html .= EBootstrap::ilabel(Yii::t('Project', ' von mindestens {least}', array(
				'{least}' => $this->least_participants,
			)), $label)." ";
		}
		
		return trim($html);
	}
	
	/**
	 * Get overview of participants for guests
	 */
	public function getParticipantsOverviewGuest() {
		$html = '';
		
		if ($this->max_participants > 0) {
			if ($this->max_participants > $this->participants + 2)
				$html .= EBootstrap::ilabel(Yii::t('Project', 'Es sind noch Plätze frei.'), 'success');
			elseif ($this->max_participants > $this->participants)
				$html .= EBootstrap::ilabel(Yii::t('Project', 'Es sind nur noch wenige Plätze frei!'), 'warning');
			else
				$html .= EBootstrap::ilabel(Yii::t('Project', 'Es sind leider keine Plätze mehr frei!'), 'important');
		}
		else {
			$html .= EBootstrap::ilabel(Yii::t('Project', 'Es sind noch Plätze frei!'), 'success');
		}
		
		return trim($html);
	}
	
	/**
	 * Check if project has a thumbnail
	 *
	 * @return boolean
	 */
	public function hasThumbnail() {
		if (count($this->images)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Returns the src of the thumbnail
	 */
	public function getThumbnailSrc($size = 'thumbnail') {
		if ($this->hasThumbnail()) {
			$path = Yii::app()->basePath.'/../images/projects/'.$this->id.'/';
			$url = Yii::app()->request->baseUrl.'/images/projects/'.$this->id.'/';
			
			foreach ($this->images as $image) {
				if (file_exists($path.$image->filename.'_'.$size))
					return $url.$image->filename.'_'.$size;
			}
		}
		
		return '';
	}
	
	/**
	 * Get the width of the thumbnail
	 *
	 * @param $size Thumbnail type
	 *
	 * @return integer
	 */
	public function getThumbnailWidth($size = 'thumbnail') {
		$formats = $this->images[0]->formats;
		$i = 0;
		
		while ($formats[$i]['title'] != $size) {
			$i++;
		}
		
		if (isset($formats[$i]['width']))
			return $formats[$i]['width'];
		else
			return 0;
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('Project', 'ID'),
			'title' => Yii::t('Project', 'Titel'),
			'slug' => Yii::t('Project', 'Permalink'),
			'description' => Yii::t('Project', 'Beschreibung'),
			'category_id' => Yii::t('Project', 'Kategorie'),
			'user_id' => Yii::t('Project', 'Admin'),
			'participantsOverview' => Yii::t('Project', 'Teilnehmer'),
			'participants' => Yii::t('Project', 'Momentane Anzahl Teilnehmer'),
			'least_participants' => Yii::t('Project', 'Mindest-Teilnehmer Zahl'),
			'max_participants' => Yii::t('Project', 'Maximal-Teilnehmer Zahl'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$t = $this->getTableAlias();

		$criteria=new CDbCriteria;

		$criteria->compare("$t.id",$this->id);
		$criteria->compare("$t.title",$this->title,true);
		$criteria->compare("$t.slug",$this->slug,true);
		$criteria->compare("$t.user_id",$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}