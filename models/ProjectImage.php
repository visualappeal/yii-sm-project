<?php

/**
 * This is the model class for table "{{projects_images}}".
 *
 * The followings are the available columns in table '{{projects_images}}':
 * @property integer $id
 * @property integer $project_id
 * @property string $title
 * @property string $description
 * @property integer $filename
 * @property integer $order_id
 */
class ProjectImage extends CActiveRecord
{
	/**
	 * Uploaded image
	 */
	public $image;
	
	/*
	public $formats = array(
		'header' => array(
			'width' => 1170,
			'height' => 220,
			'crop' => true,
		),
		'slideshow' => array(
			'width' => 1170,
			'height' => 600,
			'crop' => false,
		),
		'thumbnail' => array(
			'width' => 270,
			'height' => 270,
			'crop' => true,
		),
	);*/
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProjectImage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project_image}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('project_id, title', 'required'),
			array('image', 'required', 'on' => 'create'),
			
			array('project_id, order_id', 'numerical', 'integerOnly' => true),
			array('filename', 'length', 'max' => 32),
			array('title', 'length', 'max' => 75),
			array('extension', 'length', 'max' => 20),
			array('mime', 'length', 'max' => 50),
			array('image', 'file', 'allowEmpty' => true, 'types' => 'jpg, jpeg, png'),

			array('description', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'theProject' => array(self::BELONGS_TO, 'Project', 'project_id'),
		);
	}
	
	public function defaultScope() {
		$t = $this->getTableAlias(false, false);
		return array(
			'order' => "$t.order_id ASC",
		);
	}
	
	/**
	 * Delete image if db record is deleted
	 */
	public function beforeDelete() {
		$path = Yii::app()->basePath.'/../images/projects/'.$this->project_id.'/';
		
		foreach ($this->formats as $format) {
			$name = $format['title'];
			
			$filename = $path.$this->filename.'_'.$name;
			if (file_exists($filename))
				@unlink($filename);
		}
		
		$filename = $path.$this->filename;
		if (file_exists($filename))
			@unlink($filename);
		
		$sql = "DELETE FROM {{project_image_option}} WHERE image_id = :image_id";
		$delete = Yii::app()->db->createCommand($sql);
		$image_id = $this->id;
		$delete->bindParam(':image_id', $image_id);
		$result = $delete->execute();
		
		return parent::beforeDelete();
	}
	
	/**
	 * Get image src
	 */
	public function src($type = null) {
		$path = Yii::app()->basePath.'/../images/projects/'.$this->project_id.'/';
		$url = Yii::app()->request->baseUrl.'/images/projects/'.$this->project_id.'/';
		
		if (is_null($type))
			return $url.$this->filename;
		else {
			$type = '_'.$type;
			
			if (file_exists($path.$this->filename.$type))
				return $url.$this->filename.$type;
			else
				return $url.$this->filename;
		}
	}
	
	/**
	 * Get HTML encoded description
	 */
	public function getDescriptionEncode() {
		return nl2br(EBootstrap::encode($this->description));
	}
	
	/**
	 * Get image formats
	 */
	public function getFormats() {
		$sql = "SELECT * FROM {{project_image_format}}";
		$formats = Yii::app()->db->createCommand($sql)->queryAll();
		
		return $formats;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('Project.Image', 'ID'),
			'project_id' => Yii::t('Project.Image', 'Projekt'),
			'title' => Yii::t('Project.Image', 'Titel'),
			'description' => Yii::t('Project.Image', 'Beschreibung'),
			'filename' => Yii::t('Project.Image', 'Dateiname'),
			'extension' => Yii::t('Project.Image', 'Dateiendung'),
			'mime' => Yii::t('Project.Image', 'Dateityp'),
			'order_id' => Yii::t('Project.Image', 'Reihenfolge'),
			'image' => Yii::t('Project.Image', 'Bild'),
		);
	}
}