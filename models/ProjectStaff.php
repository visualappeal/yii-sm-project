<?php

/**
 * This is the model class for table "{{projects_participants}}".
 *
 * The followings are the available columns in table '{{projects_participants}}':
 * @property integer $id
 * @property integer $user_id
 * @property string $date_joined
 */
class ProjectStaff extends CActiveRecord
{
	/**
	 * User to add
	 */
	public $user;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProjectParticipant the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project_staff}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('project_id, date_joined, user', 'required'),
			
			array('project_id, user_id', 'numerical', 'integerOnly'=>true),
			array('user_name', 'length', 'max' => 75),

			array('id, project_id, user_id, user_name, date_joined', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'theUser' => array(self::BELONGS_TO, 'User', 'user_id'),
			'theProject' => array(self::BELONGS_TO, 'Project', 'project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('Project.Staff', 'ID'),
			'project_id' => Yii::t('Project.Staff', 'Projekt'),
			'user' => Yii::t('Project.Staff', 'Benutzer'),
			'user_id' => Yii::t('Project.Staff', 'Benutzer'),
			'user_name' => Yii::t('Project.Staff', 'Benutzer'),
			'date_joined' => Yii::t('Project.Staff', 'Hinzugefügt'),
		);
	}
	
	public function beforeValidate() {
		$this->date_joined = date('Y-m-d H:i:s');
		
		return parent::beforeSave();
	}
	
	/**
	 * Get all members
	 */
	public function getStaff() {
		$staff = $this->theProject->staff;
		
		$criteria = new CDbCriteria;
		$projectStaff = array();
		foreach ($staff as $member) {
			$projectStaff[] = $member->user_id;
		}
		$criteria->addNotInCondition('id', $projectStaff);
		
		$users = User::model()->findAll($criteria);
		return EBootstrap::listData($users, 'id', 'username');
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('date_joined',$this->date_joined,true);
		$criteria->compare('project_id', $this->project_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}