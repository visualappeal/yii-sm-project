<?php

/**
 * This is the model class for table "{{projects_dates}}".
 *
 * The followings are the available columns in table '{{projects_dates}}':
 * @property integer $id
 * @property integer $project_id
 * @property string $date_start
 * @property string $date_end
 */
class ProjectDate extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProjectDate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project_date}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('project_id, date_start, date_end', 'required'),
			array('project_id', 'numerical', 'integerOnly' => true),
			array('all_day', 'boolean'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'theProject' => array(self::BELONGS_TO, 'Project', 'project_id'),
		);
	}
	
	public function defaultScope() {
		$t = $this->getTableAlias(false, false);

		return array(
			'order' => "$t.date_start ASC",
		);
	}
	
	public function scopes() {
		$t = $this->getTableAlias();
		
		return array(
			'active' => array(
				'condition' => "$t.date_end < NOW()",
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('Project.Date', 'ID'),
			'project_id' => Yii::t('Project.Date', 'Projekt'),
			'date_start' => Yii::t('Project.Date', 'Anfang'),
			'date_end' => Yii::t('Project.Date', 'Ende'),
			'all_day' => Yii::t('Project.Date', 'Ganztägig'),
		);
	}
	
	/**
	 * Outputs date range
	 */
	public function getDateRange() {
		if (date('Y-m-d', strtotime($this->date_start)) == date('Y-m-d', strtotime($this->date_end))) {
			return $this->date_start . ' - ' . date('H:i', strtotime($this->date_end));
		}
		else {
			return $this->date_start . ' - ' . $this->date_end;
		}
	}
	
	public function beforeValidate() {
		if (!empty($this->date_start))
			$this->date_start = date('Y-m-d H:i:s', strtotime($this->date_start));
		
		if (!empty($this->date_end))
			$this->date_end = date('Y-m-d H:i:s', strtotime($this->date_end));
		
		return parent::beforeValidate();
	}
	
	public function afterFind() {
		if (!empty($this->date_start)) {
			if (date('H:i', strtotime($this->date_start)) == '00:00') {
				$this->date_start = date('d.m.Y', strtotime($this->date_start));
			}
			else {
				$this->date_start = date('d.m.Y H:i', strtotime($this->date_start));
			}
		}
		
		if (!empty($this->date_end)) {
			if (date('H:i', strtotime($this->date_end)) == '00:00') {
				$this->date_end = date('d.m.Y', strtotime($this->date_end));
			}
			else {
				$this->date_end = date('d.m.Y H:i', strtotime($this->date_end));
			}
		}
		
		return parent::afterFind();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$t = $this->getTableAlias();

		$criteria=new CDbCriteria;

		$criteria->compare("$t.id",$this->id);
		$criteria->compare("$t.project_id",$this->project_id);
		$criteria->compare("$t.date_start",$this->date_start,true);
		$criteria->compare("$t.date_end",$this->date_end,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}