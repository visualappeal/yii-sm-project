<?php

/**
 * This is the model class for table "{{projects_wiki_pages}}".
 *
 * The followings are the available columns in table '{{projects_wiki_pages}}':
 * @property integer $id
 * @property integer $wiki_id
 * @property string $title
 * @property string $slug
 * @property string $content
 */
class ProjectWikiPage extends CActiveRecord
{	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProjectWikiPage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project_wiki_page}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('wiki_id, user_id, title, content', 'required'),
			array('wiki_id, user_id, page_id', 'numerical', 'integerOnly' => true),
			
			array('title, slug', 'length', 'max' => 50),

			array('title, slug, content, ', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'theWiki' => array(self::BELONGS_TO, 'ProjectWiki', 'wiki_id'),
			'theUser' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}
	
	public function defaultScope() {
		$t = $this->getTableAlias(false, false);
		return array(
			'order' => "$t.created ASC",
		);
	}
	
	public function beforeValidate() {
		if ($this->isNewRecord) {
			$this->created = date('Y-m-d H:i:s');
			$this->user_id = Yii::app()->user->id;
		}
		else
			$this->updated = date('Y-m-d H:i:s');
		
		return parent::beforeValidate();
	}
	
	public function behaviors() {
		return array(
			'sluggable' => array(
				'class' => 'application.modules.project.extensions.behaviors.SluggableBehavior.SluggableBehavior',
				'columns' => array('title'),
				'unique' => true,
				'update' => false,
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('Project.Wiki', 'ID'),
			'wiki_id' => Yii::t('Project.Wiki', 'Wiki'),
			'user_id' => Yii::t('Project.Wiki', 'Benutzer'),
			'title' => Yii::t('Project.Wiki', 'Titel'),
			'slug' => Yii::t('Project.Wiki', 'Permalink'),
			'content' => Yii::t('Project.Wiki', 'Inhalt'),
			'created' => Yii::t('Project.Wiki', 'Erstellt'),
			'page_id' => Yii::t('Project.Wiki', 'Original Seite'),
		);
	}
}