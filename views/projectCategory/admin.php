<?php
$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte') => array('/project/project/admin'),
	Yii::t('Project.Category', 'Kategorien'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project.Category', 'Projekt-Kategorien verwalten'),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Project.Categorie', 'Erstellen'), array('create'), '', 'mini', false, 'plus', false, array('title' => Yii::t('Project.Categorie', 'Kategorie erstellen')))
		),
		'icon' => 'th-list',
		'table' => true,
	)
);

$this->widget(
	'EBootstrapGridView', array(
		'id' => 'projectCategory-grid',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => array(
			'id',
			'title',
			array(
				'class' => 'EBootstrapButtonColumn',
				'template' => '{update}{delete}'
			)
		),
	)
);

$this->endWidget();