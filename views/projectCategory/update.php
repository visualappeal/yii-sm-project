<?php
$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte') => array('/project/project/admin'),
	Yii::t('Project.Category', 'Kategorien') => array('/project/projectCategory'),
	$model->title,
	Yii::t('Project.Category', 'Bearbeiten'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project.Category', '{title}: Bearbeiten', array('{title}' => $model->title)),
		'icon' => 'pencil',
	)
);

$this->renderPartial(
	'_form', 
	array(
		'model' => $model
	)
); 

$this->endWidget();