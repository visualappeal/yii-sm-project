<?php 
$form = $this->beginWidget(
	'EBootstrapActiveForm', 
	array(
		'id' => 'projectCategory-form',
		'horizontal' => true,
	)
);
?>

	<?php echo $form->beginControlGroup($model, 'title'); ?>
		<?php echo $form->labelEx($model, 'title'); ?>
		<?php echo $form->beginControls(); ?>
			<?php echo $form->textField($model, 'title', array('maxlength' => 50)); ?>
			<?php echo $form->error($model, 'title'); ?>
		<?php echo $form->endControls($model, 'title'); ?>
	<?php echo $form->endControlGroup($model, 'title'); ?>

	<?php echo $form->beginActions(); ?>
		<?php echo EBootstrap::submitButton($model->isNewRecord ? Yii::t('Project', 'Erstellen') : Yii::t('Project', 'Speichern'), 'success', '', false, $model->isNewRecord ? 'plus' : 'ok', true); ?>
	<?php echo $form->endActions(); ?>

<?php $this->endWidget(); ?>