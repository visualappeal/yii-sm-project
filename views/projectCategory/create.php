<?php
$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte') => array('/project/project/admin'),
	Yii::t('Project.Category', 'Kategorien') => array('/project/projectCategory'),
	Yii::t('Project.Category', 'Erstellen'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project.Category', 'Kategorie erstellen'),
		'icon' => 'plus',
	)
);

$this->renderPartial(
	'_form', 
	array(
		'model' => $model
	)
); 

$this->endWidget();