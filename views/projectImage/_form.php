<?php $form=$this->beginWidget('EBootstrapActiveForm', array(
	'id' => 'projectImage-form',
	'horizontal' => true,
	'htmlOptions' => array(
		'enctype'=>'multipart/form-data'
	),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->beginControlGroup($model, 'title'); ?>
		<?php echo $form->labelEx($model, 'title'); ?>
		<?php echo $form->beginControls($model, 'title'); ?>
			<?php echo $form->textField($model, 'title'); ?>
			<?php echo $form->error($model, 'title'); ?>
		<?php echo $form->endControls($model, 'title'); ?>
	<?php echo $form->endControlGroup($model, 'title'); ?>

	<?php echo $form->beginControlGroup($model, 'description'); ?>
		<?php echo $form->labelEx($model, 'description'); ?>
		<?php echo $form->beginControls($model, 'description'); ?>
			<?php echo $form->textArea($model, 'description',array('class'=>'span5')); ?>
			<?php echo $form->error($model, 'description'); ?>
		<?php echo $form->endControls($model, 'description'); ?>
	<?php echo $form->endControlGroup($model, 'description'); ?>
	
	<?php echo $form->beginControlGroup($model, 'image'); ?>
		<?php echo $form->labelEx($model, 'image'); ?>
		<?php echo $form->beginControls($model, 'image'); ?>
			<?php echo $form->fileField($model, 'image'); ?>
			<?php echo $form->error($model, 'image'); ?>
		<?php echo $form->endControls($model, 'image'); ?>
	<?php echo $form->endControlGroup($model, 'image'); ?>

	<?php echo $form->beginActions(); ?>
		<?php echo EBootstrap::submitButton($model->isNewRecord ? Yii::t('Project.Image', 'Hinzufügen') : Yii::t('Project.Image', 'Speichern'), 'success', '', false, $model->isNewRecord ? 'plus' : 'ok', true); ?>
	<?php echo $form->endActions(); ?>

<?php $this->endWidget(); ?>