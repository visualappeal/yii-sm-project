	<tr>
		<td class="order"><?php echo EBootstrap::hiddenField('image_id[]', $image->id, array('id' => 'image_id_'.$image->id)); ?><?php echo EBootstrap::hiddenField('order_id[]', $image->order_id, array('id' => 'order_id_'.$image->id, 'size' => '3', 'maxlength' => '3', 'class' => 'project-image-order')); ?><span><?php echo $image->order_id+1; ?></span></td>
		<td class="image"><img src="<?php echo $image->src('thumbnail'); ?>" alt="<?php echo EBootstrap::encode($image->title); ?>" title="<?php echo EBootstrap::encode($image->title); ?>"></td>
		<td>
			<h4><?php echo EBootstrap::encode($image->title); ?></h4>
			<p><?php echo $image->descriptionEncode; ?></p>
		</td>
		<td>
			<?php echo EBootstrap::link('<i class="icon icon-pencil"></i>', array('/project/projectImage/update', 'id' => $image->id), array('title' => Yii::t('Project.Image', 'Bild bearbeiten'))); ?> 
			<?php echo EBootstrap::link('<i class="icon icon-trash"></i>', array('/project/projectImage/delete', 'id' => $image->id), array('title' => Yii::t('Project.Image', 'Bild löschen'))); ?>
			<?php echo EBootstrap::link('<i class="icon icon-fullscreen"></i>', array('/project/projectImage/resize', 'id' => $image->id), array('title' => Yii::t('Project.Image', 'Ausschnitte ändern'))); ?>
		</td>
	</tr>