<?php
$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte') => array('index'),
	$project->title => $this->createUrl('/project/project/viewAdmin', array('id' => $project->id)),
	Yii::t('Project.Image', 'Bilder') => array('projectImage', 'id' => $project->id),
	Yii::t('Project.Image', 'Hinzufügen'),
);
?>

<div class="widget">
	<div class="widget-decoration">
		<div class="widget-icon">
			<i class="icon icon-calendar"></i>
		</div>
		<div class="widget-title">
			<h2><?php echo Yii::t('Project.Image', 'Bild hinzufügen'); ?></h2>
		</div>
	</div>

	<div class="widget-content">
		<?php 
		$this->renderPartial(
			'_form', 
			array(
				'model'=>$model,
			)
		); 
		?>
	</div>
</div>