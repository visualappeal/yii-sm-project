<?php
$cssFile = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.project.css').'/jquery.Jcrop.min.css');
$jsFile = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.project.js').'/jquery.Jcrop.min.js');
Yii::app()->clientScript->registerScriptFile($jsFile);
Yii::app()->clientScript->registerCssFile($cssFile);

$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte') => array('/project/project/index'),
	$project->title => $this->createUrl('/project/project/viewAdmin', array('id' => $project->id)),
	Yii::t('Project.Image', 'Bild'),
	$model->title,
	Yii::t('Project.Image', 'Ausschnitt ändern'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project.Image', '{image}: Bildausschnitt ändern', array('{image}' => $model->title)),
		'icon' => 'resize-small',
	)
);

$jsCrop = '';

echo EBootstrap::beginForm(array('/project/projectimage/resize', 'id' => $model->id));

foreach ($model->formats as $format) {
	//Get default selection options
	$options = $format;
	$x1 = 0;
	$y1 = 0;
	$x2 = 0;
	$y2 = 0;
	foreach ($default as $defaultOption) {
		if ($defaultOption['format_id'] == $format['id']) {
			$x1 = $defaultOption['x1'];
			$y1 = $defaultOption['y1'];
			$x2 = $defaultOption['x2'];
			$y2 = $defaultOption['y2'];
			break;
		}
	}
	?>
	<div class="modal hide fade" id="modal-<?php echo $format['id']; ?>">
		<div class="modal-header">
			 <button class="close" data-dismiss="modal">&times;</button>
			 <h3><?php echo Yii::t('Project.Image', 'Format {format} für Bild {title} ändern', array('{format}' => $format['id'], '{title}' => $model->title)); ?></h3>
		</div>
		<div class="modal-body">
			<img src="<?php echo $image ?>" class="project-image-resize" id="project-image-resize-<?php echo $model->id; ?>-<?php echo $format['id']; ?>">
		</div>
		<div class="modal-footer">
			<a href="#" class="btn btn-primary" onclick="$('#modal-<?php echo $format['id']; ?>').modal('hide'); console.log('close')">Speichern</a>
		</div>
	</div>
	
	<p>
		<input type="hidden" value="<?php echo $x1; ?>" name="projectimagesize[<?php echo $format['id']; ?>][x1]" id="projectimagesize-<?php echo $format['id']; ?>-x1">
		<input type="hidden" value="<?php echo $y1; ?>" name="projectimagesize[<?php echo $format['id']; ?>][y1]" id="projectimagesize-<?php echo $format['id']; ?>-y1">
		<input type="hidden" value="<?php echo $x2; ?>" name="projectimagesize[<?php echo $format['id']; ?>][x2]" id="projectimagesize-<?php echo $format['id']; ?>-x2">
		<input type="hidden" value="<?php echo $y2; ?>" name="projectimagesize[<?php echo $format['id']; ?>][y2]" id="projectimagesize-<?php echo $format['id']; ?>-y2">
		<a class="btn" data-toggle="modal" href="#modal-<?php echo $format['id']; ?>"><?php echo ucfirst($format['title']); ?></a>
	</p>
	
	<?php 
		$jsCrop .= '
			var width = document.outerWidth/2 || 600;
			var height = document.outerHeight/2 || 400;
			$("#project-image-resize-'.$model->id.'-'.$format['id'].'").Jcrop({
				bgColor: "black",
				bgOpacity: .4,
				aspectRatio: '.$options['width'].'/'.$options['height'].',
				minSize: ['.$options['width'].', '.$options['height'].'],
				boxWidth: width,
				boxHeight: height,';
		
		if ($x2 > 0 and $y2 > 0) {
			$jsCrop .= '
				setSelect: ['.$x1.', '.$y1.', '.$x2.', '.$y2.'],';
		}
		
		$jsCrop .= '
			onChange: function (coord) {
				$("#projectimagesize-'.$format['id'].'-x1").val(coord.x);
				$("#projectimagesize-'.$format['id'].'-y1").val(coord.y);
				$("#projectimagesize-'.$format['id'].'-x2").val(coord.x2);
				$("#projectimagesize-'.$format['id'].'-y2").val(coord.y2);
			}
		});
		';
}

?>

<div class="form-actions">
	<?php echo EBootstrap::submitButton(Yii::t('Project.Image', 'Speichern'), 'success', '', false, 'ok', true, array('name' => 'resize', 'id' => 'resize')) ?>
</div>

<?php echo EBootstrap::endForm(); ?>

<?php $this->endWidget(); ?>

<?php Yii::app()->clientScript->registerScript('project-image-resize-' . $model->id, $jsCrop, CClientScript::POS_READY); ?>