<?php
$this->breadcrumbs=array(
	Yii::t('Core', 'Verwaltung') => $this->createUrl('/admin/index'),
	Yii::t('Project', 'Projekte') => $this->createUrl('/project/project/index'),
	$project->title => $this->createUrl('/project/project/viewTitle', array('title' => $project->slug)),
	Yii::t('Project.Image', 'Bilder')
);
?>

<h2><?php echo Yii::t('Project.Image', 'Bild wirklich löschen?'); ?></h2>

<?php 
	$this->renderPartial('/project/_menu', array(
		'model' => $project,
		'active' => 'images',
	));
?>

<p>
	<?php echo Yii::t('Project.Image', 'Soll das Bild wirklich gelöscht werden?') ?>
</p>

<p>
	<?php echo EBootstrap::ibutton(Yii::t('Project.Image', 'Abbrechen'), $this->createUrl('/project/projectImage/view', array('id' => $project->id))); ?> 
	<?php echo EBootstrap::ibutton(Yii::t('Project.Image', 'Löschen'), $this->createUrl('/project/projectImage/deleteFile', array('id' => $project->id, 'hash' => $hash, 'confirm' => true)), 'danger', '', false, 'trash', true); ?>
</p>