<?php
$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte') => array('/project/project/index'),
	$project->title => $this->createUrl('/project/project/viewAdmin', array('id' => $project->id)),
	Yii::t('Project.Image', 'Bilder'),
	$model->title,
	Yii::t('Project.Image', 'Bearbeiten'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project.Image', '{image}: bearbeiten', array('{image}' => $model->title)),
		'icon' => 'pencil',
	)
);

$this->renderPartial(
	'_form', 
	array(
		'model' => $model,
	)
); 

$this->endWidget();