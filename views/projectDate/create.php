<?php
$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte') => array('index'),
	$project->title => $this->createUrl('/project/project/viewAdmin', array('id' => $project->id)),
	Yii::t('Project.Date', 'Termine'),
	Yii::t('Project.Date', 'Hinzufügen'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project.Date', 'Termin hinzufügen'),
		'icon' => 'plus',
	)
);

$this->renderPartial(
	'_form', 
	array(
		'model' => $model
	)
); 

$this->endWidget();