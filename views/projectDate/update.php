<?php
$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte') => array('/project/project/admin'),
	$project->title => array('/project/project/viewAdmin', 'id' => $project->id),
	Yii::t('Project.Date', 'Termine') => array('projectDate', 'id' => $project->id),
	Yii::t('Project.Date', 'Bearbeiten'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project.Date', 'Termin bearbeiten'),
		'icon' => 'pencil',
	)
);

$this->renderPartial(
	'_form', 
	array(
		'model' => $model
	)
); 

$this->endWidget();