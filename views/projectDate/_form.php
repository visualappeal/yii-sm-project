<?php
//Datetime-Picker
$jsFile = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.project.js').'/timepicker.js');
Yii::app()->clientScript->registerScriptFile($jsFile, CClientScript::POS_END);

Yii::app()->clientScript->registerScript('datetimepicker', "
$.timepicker.regional['de'] = {
  timeOnlyTitle: 'Uhrzeit auswählen',
  timeText: 'Zeit',
  hourText: 'Stunde',
  minuteText: 'Minute',
  secondText: 'Sekunde',
  currentText: 'Jetzt',
  closeText: 'Auswählen',
  ampm: false,
  dateFormat: 'dd.mm.yy',
  timeFormat: 'hh:mm:ss',
};

$.timepicker.setDefaults($.timepicker.regional['de']);

$('.timepicker').datetimepicker();
", CClientScript::POS_READY);
?>

<?php 
$form = $this->beginWidget(
	'EBootstrapActiveForm', 
	array(
		'id' => 'project-form',
		'horizontal' => true,
	)
); 
?>

	<?php echo $form->beginControlGroup($model, 'date_start'); ?>
		<?php echo $form->labelEx($model, 'date_start'); ?>
		<?php echo $form->beginControls($model, 'date_start'); ?>
			<?php echo $form->textField($model, 'date_start',array('class'=>'timepicker')); ?>
			<?php echo $form->error($model, 'date_start'); ?>
		<?php echo $form->endControls($model, 'date_start'); ?>
	<?php echo $form->endControlGroup($model, 'date_start'); ?>

	<?php echo $form->beginControlGroup($model, 'date_end'); ?>
		<?php echo $form->labelEx($model, 'date_end'); ?>
		<?php echo $form->beginControls($model, 'date_end'); ?>
			<?php echo $form->textField($model, 'date_end',array('class'=>'timepicker')); ?>
			<?php echo $form->error($model, 'date_end'); ?>
		<?php echo $form->endControls($model, 'date_end'); ?>
	<?php echo $form->endControlGroup($model, 'date_end'); ?>
	
	<?php echo $form->beginControlGroup($model, 'all_day'); ?>
		<?php echo $form->labelEx($model, 'all_day'); ?>
		<?php echo $form->beginControls($model, 'all_day'); ?>
			<?php echo $form->checkBox($model, 'all_day'); ?>
			<?php echo $form->error($model, 'all_day'); ?>
		<?php echo $form->endControls($model, 'all_day'); ?>
	<?php echo $form->endControlGroup($model, 'all_day'); ?>

	<?php echo $form->beginActions(); ?>
		<?php echo EBootstrap::submitButton($model->isNewRecord ? Yii::t('Project.Date', 'Hinzufügen') : Yii::t('Project.Date', 'Speichern'), 'success', '', false, $model->isNewRecord ? 'plus' : 'ok', true); ?>
	<?php echo $form->endActions(); ?>

<?php $this->endWidget(); ?>