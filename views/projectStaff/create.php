<?php
$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte') => array('/project/project/index'),
	$project->title => array('/project/project/viewAdmin', 'id' => $project->id),
	Yii::t('Project.Staff', 'Mitarbeiter'),
	Yii::t('Project.Staff', 'Hinzufügen'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project.Staff', 'Mitarbeiter hinzufügen'),
		'icon' => 'plus',
	)
);

echo $this->renderPartial(
	'_form', 
	array(
		'model' => $model,
		'users' => $users,
	)
); 

$this->endWidget();