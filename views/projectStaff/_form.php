<?php 
$form = $this->beginWidget(
	'EBootstrapActiveForm', 
	array(
		'id' => 'project-form',
		'horizontal' => true,
	)
); 
?>
	
	<?php echo $form->beginControlGroup($model, 'user'); ?>
		<?php echo $form->labelEx($model, 'user'); ?>
		<?php echo $form->beginControls($model, 'user'); ?>
			<?php $this->widget('application.modules.project.widgets.EUserSelect.EUserSelect', array(
				'valueElementId' => 'ProjectStaff_user',
				'users' => $users,
				'allowNew' => true,
				'submitId' => 'add-staff-member',
			)); ?>
			<?php echo $form->hiddenField($model, 'user'); ?>
			<?php echo $form->error($model, 'user'); ?>
		<?php echo $form->endControls($model, 'user'); ?>
	<?php echo $form->endControlGroup($model, 'user'); ?>

	<?php echo $form->beginActions(); ?>
		<?php echo EBootstrap::submitButton(Yii::t('Project.Staff', 'Hinzufügen'), 'success', '', false, 'plus', true); ?>
	<?php echo $form->endActions(); ?>

<?php $this->endWidget(); ?>