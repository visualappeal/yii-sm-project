<?php
$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte') => array('admin'),
	Yii::t('Project', 'Erstellen'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project', 'Projekt erstellen'),
		'icon' => 'plus',
	)
);

$this->renderPartial(
	'_form', 
	array(
		'model' => $model
	)
); 

$this->endWidget();