<?php
$form = $this->beginWidget('EBootstrapActiveForm', array(
	'id' => 'project-form',
	'horizontal' => true,
)); 
?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->bootstrapTextField($model, 'title'); ?>

	<?php echo $form->bootstrapTextField($model, 'slug', array('class' => 'slug', 'data-title' => 'Project_title')); ?>
	
	<?php echo $form->bootstrapTextArea($model, 'description'); ?>
	
	<?php echo $form->beginControlGroup($model, 'category_id'); ?>
		<?php echo $form->labelEx($model, 'category_id'); ?>
		<?php echo $form->beginControls($model, 'category_id'); ?>
			<?php echo $form->dropDownList($model, 'category_id', $model->categories); ?>
			<?php echo $form->error($model, 'category_id'); ?>
		<?php echo $form->endControls($model, 'category_id'); ?>
	<?php echo $form->endControlGroup($model, 'category_id'); ?>
	
	<?php echo $form->bootstrapTextField($model, 'participants'); ?>
	
	<?php echo $form->bootstrapTextField($model, 'least_participants'); ?>
	
	<?php echo $form->bootstrapTextField($model, 'max_participants'); ?>

	<?php echo $form->beginActions(); ?>
		<?php echo EBootstrap::submitButton($model->isNewRecord ? Yii::t('Project', 'Erstellen') : Yii::t('Project', 'Speichern'), 'success', '', false, $model->isNewRecord ? 'plus' : 'ok', true); ?>
	<?php echo $form->endActions(); ?>

<?php $this->endWidget(); ?>