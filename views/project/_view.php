<div class="row">
	<div class="col-sm-3 project-sidebar portlet">
		<?php if ($data->hasThumbnail()): ?>
			<p>
				<a href="<?php echo $this->createUrl('/project/project/viewTitle', array('title' => $data->slug)); ?>">
					<img src="<?php echo $data->thumbnailSrc; ?>" class="thumbnail">
				</a>
			</p>
		<?php endif; ?>
		
		<?php if (count($data->dates)): ?>
			<h3 class="portlet-title"><?php echo EBootstrap::icon('calendar'); ?> <?php echo Yii::t('Project', 'Termine'); ?></h3>
			<ul>
				<?php foreach ($data->dates as $date): ?>
					<li><?php echo $date->dateRange; ?></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>

		<?php if ((Yii::app()->user->isStaff()) and (count($data->staff))): ?>
			<h3 class="portlet-title"><?php echo Yii::t('Project', 'Mitarbeiter'); ?></h3>
			<ul>
				<?php foreach ($data->staff as $staff): ?>
					<li><?php echo isset($staff->theUser->id) ? EBootstrap::link($staff->theUser->name, array('/user/user/profile', 'id' => $staff->theUser->id)) : $staff->user_name; ?></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	
	</div>
	
	<div class="col-sm-9 project-content">

		<h2><?php echo EBootstrap::link($data->title, array('/project/project/viewTitle', 'title' => $data->slug)); ?></h2>
		
		<p>
			<strong><?php echo Yii::t('Project', 'Teilnehmer'); ?></strong>: 
			<?php if (!Yii::app()->user->isGuest): ?>
				<?php echo $data->participantsOverview; ?>
			<?php else: ?>
				<?php echo $data->participantsOverviewGuest; ?>
			<?php endif; ?>
		</p>
		
		<?php if (!empty($data->description)): ?>
			<p>
				<?php echo $data->descriptionEncode; ?>
			</p>
		<?php endif; ?>

	</div>
</div>