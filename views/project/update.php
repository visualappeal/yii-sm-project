<?php
$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte') => array('admin'),
	$model->title => array('viewAdmin', 'id' => $model->id),
	Yii::t('Project', 'Bearbeiten'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project', 'Bearbeiten: <em>{project}</em>', array('{project}' => $model->title)),
		'icon' => 'pencil',
	)
);

$this->renderPartial(
	'_form', 
	array(
		'model' => $model
	)
); 

$this->endWidget();