<?php
$this->title[] = Yii::t('Project', 'Projekte');

$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte'),
);
?>

<h2><?php echo Yii::t('Project', 'Projekte {date}', array('{date}' => $curDate)) ?></h2>

<div id="project-index">
	<?php if (count($projects)): ?>
		<?php foreach ($projects as $project): ?>
			<div id="project-<?php echo $project->id; ?>" class="project">
				<?php
					$this->renderPartial('_view', array(
						'data' => $project,
					));
				?>
			</div>
		<?php endforeach; ?>
	<?php else: ?>
		<?php echo EBootstrap::ilabel(Yii::t('Project', 'Zur Zeit gibt es keine Projekte, Events oder Gruppen!'), 'info'); ?>
	<?php endif; ?>
</div>