<?php
$this->title[] = $model->title;

$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte') => $this->createUrl('/project/project/index'),
	$model->title,
);
?>

<div id="project-view">
	<?php if ($model->hasThumbnail()): ?>
		<p id="project-headline">
			<img src="<?php echo $model->getThumbnailSrc('header'); ?>" width="<?php echo $model->getThumbnailWidth('header'); ?>">
		</p>
	<?php endif; ?>
	
	<div class="row">
		<div class="span3 project-sidebar portlet">

			<?php if (count($model->dates)): ?>
				<h3 class="portlet-title"><?php echo Yii::t('Project', 'Termine'); ?></h3>
				<ul class="unstyled">
					<?php foreach ($model->dates as $date): ?>
						<li><?php echo $date->dateRange; ?></li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
			<p>
				<?php if (Yii::app()->user->isStaff()): ?>
					<?php echo EBootstrap::ibutton(Yii::t('Project.Date', 'Termin'), array('/project/projectDate/create', 'id' => $model->id), 'success', '', false, 'plus', true); ?></li>
				<?php endif; ?>
			</p>
	
			<?php if ((Yii::app()->user->isStaff()) and (count($model->staff))): ?>
				<h3 class="portlet-title"><?php echo Yii::t('Project', 'Mitarbeiter'); ?></h3>
				<ul class="unstyled">
					<?php foreach ($model->staff as $staff): ?>
						<li><?php echo isset($staff->theUser->id) ? EBootstrap::link($staff->theUser->name, array('/user/user/profile', 'id' => $staff->theUser->id)) : $staff->user_name; ?></li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
			
			<p>
				<?php if (Yii::app()->user->isStaff()): ?>
					<?php echo EBootstrap::ibutton(Yii::t('Project.Staff', 'Mitarbeiter'), array('/project/projectStaff/create', 'id' => $model->id), 'success', '', false, 'plus', true); ?></li>
				<?php endif; ?>
			</p>

		</div>
		
		<div id="project-content" class="span6">
			<h2><?php echo $model->title; ?></h2>
			
			<p>
				<strong><?php echo Yii::t('Project', 'Teilnehmer'); ?></strong>: 
				<?php if (!Yii::app()->user->isGuest): ?>
					<?php echo $model->participantsOverview; ?>
				<?php else: ?>
					<?php echo $model->participantsOverviewGuest; ?>
				<?php endif; ?>
			</p>
			
			<?php if (!empty($model->description)): ?>
				<p>
					<?php echo $model->descriptionEncode; ?>
				</p>
			<?php endif; ?>
			
			<?php if (Yii::app()->user->isStaff()): ?>
				<p>
					<?php echo EBootstrap::ibutton(Yii::t('Project', 'Projekt bearbeiten'), array('/project/project/viewAdmin', 'id' => $model->id), 'default', '', false, 'pencil'); ?>
				</p>
			<?php endif; ?>
	
		</div>
		
		<div class="span3">
			<?php
				$this->widget('application.modules.project.portlets.Calendar', array(
					'month' => date('m'),
		        	'year' => date('Y'),
		        	'project' => $model->id
				));
				
				$this->widget('application.modules.project.portlets.Projects');
			?>
		</div>
	</div>
</div>