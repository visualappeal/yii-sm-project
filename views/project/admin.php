<?php
$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte'),
);
 
$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project', 'Projekte verwalten'),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Project', 'Erstellen'), array('/project/project/create'), '', 'mini', '', 'plus', false, array('title' => Yii::t('Project', 'Projekt erstellen')))
		),
		'icon' => 'th-list',
		'table' => true,
	)
);

$this->widget(
	'EBootstrapGridView', 
	array(
		'id' => 'project-grid',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => array(
			'id',
			'title',
			array(
				'name' => 'participantsOverview',
				'value' => '$data->participantsOverview',
				'type' => 'html',
				'filter' => false,
			),
			array(
				'class'=>'EBootstrapButtonColumn',
				'template' => '{viewAdmin}{update}{delete}',
				'buttons' => array(
					'viewAdmin' => array(
						'label' => '<icon class="icon icon-search" title="'.Yii::t('Project', 'Projekt ansehen').'"></i>',
						'url' => 'Yii::app()->createUrl("/project/project/viewAdmin", array("id" => $data->id))',
						'imageUrl' => false,
					),
				),
			),
		),
		'summaryText' => false,
	)
); 

$this->endWidget();