<?php
$this->breadcrumbs = array(
	Yii::t('Project', 'Projekte') => $this->createUrl('/project/project/admin'),
	$model->title,
);

$this->title[] = $model->title;

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => $model->title,
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Project', 'Bearbeiten'), array('/project/project/update', 'id' => $model->id), '', 'mini', false, 'pencil', false, array('title' => Yii::t('Project', 'Projekt bearbeiten'))),
			EBootstrap::ibutton(Yii::t('Project.Wiki', 'Wiki'), array('/project/projectWiki/view', 'id' => $model->id), '', 'mini', false, 'edit', false, array('title' => Yii::t('Project.Wiki', 'Wiki ansehen'))),
		),
		'icon' => 'search',
	)
);
?>

<p>
	<strong><?php echo Yii::t('Project', 'Teilnehmer'); ?></strong>: <?php echo $model->participantsOverview; ?>
</p>

<?php
$this->endWidget();

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project.Date', 'Termine'),
		'buttons' => EBootstrap::buttonGroup(
			array(
				EBootstrap::ibutton(Yii::t('Project.Date', 'Erstellen'), $this->createUrl('/project/projectDate/create', array('id' => $model->id)), '', 'mini', false, 'plus', false, array('title' => Yii::t('Project.Date', 'Termin erstellen')))
			)
		),
		'icon' => 'calendar',
		'table' => true,
	)
);

$this->widget(
	'EBootstrapGridView', 
	array(
		'id' => 'projectDate-grid',
		'dataProvider' => $dates->search(),
		'columns' => array(
			'date_start',
			'date_end',
			array(
				'name' => 'all_day',
				'value' => '($data->all_day) ? Yii::t("Project.Date", "Ja") : Yii::t("Project.Date", "Nein")'
			),
			array(
				'class'=>'EBootstrapButtonColumn',
				'template' => '{update}{delete}',
				'updateButtonUrl' => 'Yii::app()->createAbsoluteUrl("/project/projectDate/update", array("id" => $data->id))',
				'deleteButtonUrl' => 'Yii::app()->createAbsoluteUrl("/project/projectDate/delete", array("id" => $data->id))'
			),
		),
		'summaryText' => false,
	)
); 

$this->endWidget();

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project.Staff', 'Mitarbeiter'),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Project.Staff', 'Hinzufügen'), $this->createUrl('/project/projectStaff/create', array('id' => $model->id)), '', 'mini', false, 'plus', false, array('title' => Yii::t('Project.Staff', 'Mitarbeiter hinzufügen')))
		),
		'icon' => 'user',
		'table' => true,
	)
);

$this->widget(
	'EBootstrapGridView', 
	array(
		'id' => 'projectStaff-grid',
		'dataProvider' => $staff->search(),
		'columns' => array(
			array(
				'name' => 'user_id',
				'type' => 'raw',
				'value' => '(isset($data->theUser)) ? $data->theUser->name : "<em>".Yii::t("Project.Staff", "Benutzer gelöscht")."</em>"',
			),
			array(
				'class'=>'EBootstrapButtonColumn',
				'template' => '{delete}',
				'deleteButtonUrl' => 'Yii::app()->createAbsoluteUrl("/project/projectStaff/delete", array("id" => $data->id))',
			),
		),
	)
); 

$this->endWidget();

//Draggable list order
Yii::app()->clientScript->registerScript('project-image-list-order', '
	$(function() {
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};
		
		$( "#project-image-admin tbody" ).sortable({
			cursor: "move",
			helper: fixHelper,
			containment: "#project-image-admin tbody",
			update: function(event, ui) {
				$("#project-image-admin tbody tr").each(function(index) {
					$(this).children(".order").children(".project-image-order").val(index);
					$(this).children(".order").children("span").html(index+1);
				});
				
				$.ajax({
					url: "'.Yii::app()->createAbsoluteUrl('/project/projectImage/order', array('id' => $model->id)).'",
					type: "post",
					data: $("#project-image-form").serialize()
				});
			}
		}).disableSelection();
	});
');

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Project.Image', 'Bilder'),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Project.Image', 'Hochladen'), $this->createUrl('/project/projectImage/create', array('id' => $model->id)), '', 'mini', false, 'upload', false, array('title' => Yii::t('Project.Image', 'Bild hochladen')))
		),
		'icon' => 'picture',
		'table' => true,
	)
);
?>

<?php if (count($images)): ?>
	<?php echo EBootstrap::beginForm('', 'post', array('id' => 'project-image-form')); ?>
	<table class="table" id="project-image-admin">
		<thead>
			<tr>
				<th class="order"><?php echo Yii::t('Project.Image', '#'); ?></th>
				<th class="image"><?php echo Yii::t('Project.Image', 'Vorschau'); ?></th>
				<th><?php echo Yii::t('Project.Image', 'Beschreibung'); ?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($images as $image): ?>
				<?php 
				$this->renderPartial(
					'/projectImage/_adminView', 
					array(
						'image' => $image,
					)
				);
				?>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php echo EBootstrap::endForm(); ?>
<?php else: ?>
	<p>
		<?php echo EBootstrap::ilabel(Yii::t('Project.Image', 'Es wurden für das Projekt noch keine Bilder hochgeladen.'), 'info'); ?>
	</p>
<?php endif; ?>

<?php $this->endWidget();