<?php
$this->breadcrumbs=array(
	Yii::t('Core', 'Verwaltung') => $this->createUrl('/admin/index'),
	Yii::t('Project', 'Projekte') => $this->createUrl('/project/project/index'),
	$model->theProject->title => $this->createUrl('/project/viewTitle', array('title' => $model->theProject->slug)),
	Yii::t('Project.Wiki', 'Wiki')
);
?>

<h2><?php echo Yii::t('Project.Wiki', '{project} - Wiki', array('{project}' => $model->theProject->title)); ?></h2>

<?php 
$this->renderPartial('/projectWiki/_menu', array(
	'wiki' => $model,
	'pages' => $model->pages,
	'active' => 0,
));

if (!count($model->pages)): ?>
	<p>
		<?php echo EBootstrap::ilabel(Yii::t('Project.Wiki', 'Es wurden noch keine Seiten erstellt'), 'info'); ?>
	</p>
<?php endif; ?>