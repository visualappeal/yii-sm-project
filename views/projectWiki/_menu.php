<ul class="nav nav-tabs project-wiki">
	<?php foreach ($pages as $page): ?>
		<li<?php if ($page->id == $active): ?> class="active"<?php endif; ?>>
			<?php echo EBootstrap::link($page->title, array('/project/projectWikiPage/view', 'id' => $page->id)); ?>
		</li>
	<?php endforeach; ?>
	
	<li class="project-wiki-nav-create<?php if ($active == 'create'): ?> active<?php endif; ?>">
		<?php echo EBootstrap::link('<i class="icon icon-plus"></i>', array('/project/projectWikiPage/create', 'id' => $wiki->id)); ?>
	</li>

	<li class="project-wiki-nav-delete<?php if ($active == 'delete'): ?> active<?php endif; ?>">
		<?php echo EBootstrap::link('<i class="icon icon-trash"></i>', array('/project/projectWikiPage/delete', 'id' => $active)); ?>
	</li>
</ul>