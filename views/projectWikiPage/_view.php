<div class="wiki-page" id="wiki-page-<?php echo $model->id; ?>">
	<?php echo $model->content; ?>

	<p>
		<?php echo EBootstrap::link(EBootstrap::icon('pencil') . ' ' . Yii::t('Project.Wiki', 'Seite bearbeiten'), array('/project/projectWikiPage/update', 'id' => $model->id)); ?>
	</p>
</div>