<?php
$this->breadcrumbs = array(
	Yii::t('Project', 'Projekte') => array('index'),
	$model->theWiki->theProject->title => array('project/project/viewAdmin', 'id' => $model->theWiki->theProject->id),
	Yii::t('Project.Wiki', 'Wiki') => array('/project/projectWiki', 'id' => $model->theWiki->id),
	Yii::t('Project.Wiki', 'Seite erstellen'),
);
?>

<div class="widget-tab">
	<div class="widget-decoration">
		<div class="widget-title">
			<h2><?php echo Yii::t('Project.Wiki', '{project}: Seite erstellen', array('{project}' => $model->theWiki->theProject->title)); ?></h2>
		</div>
		<div class="widget-tabs">
			<?php 
			$this->renderPartial('/projectWiki/_menu', 
				array(
					'wiki' => $model->theWiki,
					'pages' => $model->theWiki->pages,
					'active' => 'create',
				)
			);
			?>
		</div>
	</div>

	<div class="widget-content">
		<h3><?php echo $model->title; ?></h3>

		<?php
		$this->renderPartial(
			'/projectWikiPage/_form', 
			array(
				'model' => $model,
			)
		);
		?>
	</div>
</div>