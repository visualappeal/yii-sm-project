<?php
$this->breadcrumbs=array(
	Yii::t('Project', 'Projekte') => array('index'),
	$model->theWiki->theProject->title => array('/project/project/viewAdmin', 'id' => $model->theWiki->theProject->id),
	Yii::t('Project.Wiki', 'Wiki') => array('/project/projectWiki', 'id' => $model->theWiki->id),
	$model->title,
);
?>

<div class="widget-tab">
	<div class="widget-decoration">
		<div class="widget-title">
			<h2><?php echo $model->theWiki->theProject->title; ?></h2>
		</div>
		<div class="widget-tabs">
			<?php 
			$this->renderPartial('/projectWiki/_menu', 
				array(
					'wiki' => $model->theWiki,
					'pages' => $model->theWiki->pages,
					'active' => $model->id,
				)
			);
			?>
		</div>
	</div>

	<div class="widget-content">
		<h3><?php echo $model->title; ?></h3>

		<?php
		$this->renderPartial(
			'/projectWikiPage/_view', 
			array(
				'model' => $model,
			)
		);
		?>
	</div>
</div>