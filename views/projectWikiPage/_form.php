<?php 
$cs = Yii::app()->clientScript;

$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/redactor/redactor.min.js');
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/redactor/de.js');
$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/redactor/fullscreen.js');
$cs->registerCssFile(Yii::app()->request->baseUrl . '/js/redactor/redactor.css');

$form = $this->beginWidget(
	'EBootstrapActiveForm', 
	array(
		'id' => 'projectWikiPage-form',
		'horizontal' => true,
	)
);
?>

<?php echo $form->beginControlGroup($model, 'title'); ?>
	<?php echo $form->labelEx($model, 'title'); ?>
	<?php echo $form->beginControls(); ?>
		<?php echo $form->textField($model, 'title'); ?>
		<?php echo $form->error($model, 'title'); ?>
	<?php echo $form->endControls($model, 'title'); ?>
<?php echo $form->endControlGroup($model, 'title'); ?>

<?php echo $form->beginControlGroup($model, 'slug'); ?>
	<?php echo $form->labelEx($model, 'slug'); ?>
	<?php echo $form->beginControls($model, 'slug'); ?>
		<?php echo $form->textField($model, 'slug', array('class' => 'slug', 'data-title' => 'ProjectWikiPage_title')); ?>
		<?php echo $form->error($model, 'slug'); ?>
	<?php echo $form->endControls($model, 'slug'); ?>
<?php echo $form->endControlGroup($model, 'slug'); ?>

<?php echo $form->beginControlGroup($model, 'content'); ?>
	<?php echo $form->labelEx($model, 'content'); ?>
	<?php echo $form->beginControls($model, 'content'); ?>
		<?php echo $form->textArea($model, 'content', array('class' => 'texteditor', 'rows' => 20)); ?>
		<?php echo $form->error($model, 'content'); ?>
	<?php echo $form->endControls($model, 'content'); ?>
<?php echo $form->endControlGroup($model, 'content'); ?>

<?php echo $form->beginActions(); ?>
	<?php echo EBootstrap::submitButton($model->isNewRecord ? Yii::t('Project.Staff', 'Erstellen') : Yii::t('Project.Staff', 'Speichern'), 'success', '', false, $model->isNewRecord ? 'plus' : 'ok', true); ?>
<?php echo $form->endActions(); ?>

<?php $this->endWidget(); ?>