<?php

/**
 * Erste Migration um notwendige Tabellen zu erstellen.
 */
class m130823_082630_init extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'{{project_category}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'title' => 'varchar(50) NOT NULL',
				'PRIMARY KEY (`id`)',
			)
		);

		$this->createTable(
			'{{project}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'title' => 'varchar(70) NOT NULL',
				'slug' => 'varchar(70) NOT NULL',
				'description' => 'text NOT NULL',
				'category_id' => 'int(10) unsigned NOT NULL',
				'user_id' => 'int(10) unsigned NOT NULL',
				'participants' => 'smallint(5) unsigned NOT NULL',
				'least_participants' => 'smallint(5) unsigned NOT NULL',
				'max_participants' => 'smallint(5) unsigned NOT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `slug` (`slug`)',
				'KEY `category_id` (`category_id`)',
				'KEY `user_id` (`user_id`)'
			)
		);

		$this->createTable(
			'{{project_date}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'project_id' => 'int(10) unsigned NOT NULL',
				'date_start' => 'datetime NOT NULL',
				'date_end' => 'datetime NOT NULL',
				'all_day' => 'tinyint(1) NOT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `project_id` (`project_id`)',
			)
		);

		$this->createTable(
			'{{project_image}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'project_id' => 'int(10) unsigned NOT NULL',
				'title' => 'varchar(75) NOT NULL',
				'description' => 'text NOT NULL',
				'filename' => 'varchar(32) NOT NULL',
				'extension' => 'varchar(20) NOT NULL',
				'mime' => 'varchar(50) NOT NULL',
				'order_id' => 'smallint(5) unsigned NOT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `project_id` (`project_id`)',
				'KEY `order_id` (`order_id`)',
			)
		);

		$this->createTable(
			'{{project_image_format}}',
			array(
				'id' => 'smallint(5) unsigned NOT NULL AUTO_INCREMENT',
				'title' => 'varchar(30) NOT NULL',
				'width' => 'smallint(5) unsigned NOT NULL',
				'height' => 'smallint(5) unsigned NOT NULL',
				'crop' => 'tinyint(1) NOT NULL',
				'PRIMARY KEY (`id`)',
			)
		);

		$this->createTable(
			'{{project_image_option}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'image_id' => 'int(10) unsigned NOT NULL',
				'format_id' => 'tinyint(3) unsigned NOT NULL',
				'x1' => 'smallint(5) unsigned NOT NULL',
				'y1' => 'smallint(5) unsigned NOT NULL',
				'x2' => 'smallint(5) unsigned NOT NULL',
				'y2' => 'smallint(5) unsigned NOT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `image_id` (`image_id`)',
				'KEY `format_id` (`format_id`)',
			)
		);

		$this->createTable(
			'{{project_staff}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'project_id' => 'int(10) unsigned NOT NULL',
				'user_id' => 'int(10) unsigned NOT NULL',
				'user_name' => 'int(75) NOT NULL',
				'date_joined' => 'datetime NOT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `project_id` (`project_id`)',
				'KEY `user_id` (`user_id`)',
			)
		);

		$this->createTable(
			'{{project_wiki}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'project_id' => 'int(10) unsigned NOT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `project_id` (`project_id`)',
			)
		);

		$this->createTable(
			'{{project_wiki_page}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'wiki_id' => 'int(10) unsigned NOT NULL',
				'user_id' => 'int(10) unsigned NOT NULL',
				'title' => 'varchar(50) NOT NULL',
				'slug' => 'varchar(50) NOT NULL',
				'content' => 'text NOT NULL',
				'created' => 'datetime NOT NULL',
				'updated' => 'datetime NOT NULL',
				'page_id' => 'int(10) unsigned NOT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `wiki_id` (`wiki_id`)',
				'KEY `user_id` (`user_id`)',
				'KEY `page_id` (`page_id`)',
			)
		);
	}

	public function safeDown()
	{
		$this->dropTable('{{project_wiki_page}}');
		$this->dropTable('{{project_wiki}}');
		$this->dropTable('{{project_staff}}');
		$this->dropTable('{{project_image_option}}');
		$this->dropTable('{{project_image_format}}');
		$this->dropTable('{{project_image}}');
		$this->dropTable('{{project_date}}');
		$this->dropTable('{{project}}');
		$this->dropTable('{{project_category}}');
	}
}