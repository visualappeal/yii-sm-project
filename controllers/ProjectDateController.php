<?php

/**
 * Project date controller.
 *
 * @package Project
 * @subpackage Date
 */
class ProjectDateController extends Controller
{
	/**
	 * Default layout.
	 *
	 * @access public
	 * @var string
	 */
	public $layout='//layouts/admin';
	
	public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @access public
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('create', 'update', 'delete'),
				'roles' => array(User::LEVEL_BOARD),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Creates a new date.
	 *
	 * @param integer $id Project ID
	 */
	public function actionCreate($id)
	{
		$project = Project::model()->findByPk($id);
		
		if (is_null($project))
			throw new CHttpException(404);
		
		$model = new ProjectDate;
		$model->project_id = $project->id;

		if (isset($_POST['ProjectDate'])) {
			$model->attributes = $_POST['ProjectDate'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('Project.Date', 'Neuen Termin erstellt.'));
				$this->redirect(array('/project/project/viewAdmin', 'id' => $project->id));
			}
		}

		$this->render(
			'create',
			array(
				'model' => $model,
				'project' => $project,
			)
		);
	}
	
	/**
	 * Updates a date.
	 *
	 * @param integer $id ProjectDate ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$model = ProjectDate::model()->findByPk($id);
		$project = $model->theProject;
		
		if (is_null($model))
			throw new CHttpException(404);
		
		if (!isset($project) or empty($project))
			throw new CHttpException(500);

		if (isset($_POST['ProjectDate'])) {
			$model->attributes = $_POST['ProjectDate'];

			if($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('Project.Date', 'Der Termin wurde erfolgreich bearbeitet.'));
				$this->redirect(array('/project/project/viewAdmin', 'id' => $project->id));
			}
		}

		$this->render(
			'update',
			array(
				'model' => $model,
				'project' => $project,
			)
		);
	}
	
	/**
	 * Delete date from project.
	 *
	 * @param integer $id ProjectDate ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionDelete($id) {
		$model = ProjectDate::model()->findByPk($id);
		$project = $model->theProject;
		
		if (is_null($model))
			throw new CHttpException(404);
		
		$model->delete();
		
		Yii::app()->user->setFlash('info', Yii::t('Project.Date', 'Der Termin wurde gelöscht.'));
		$this->redirect(array('/project/project/viewAdmin', 'id' => $project->id));
	}
}
