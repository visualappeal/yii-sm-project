<?php

class ProjectController extends Controller
{
	/**
	 * Default layout.
	 *
	 * @access public
	 * @var string the default layout for the views.
	 */
	public $layout = '//layouts/admin';

	/**
	 * Get the controller filters.
	 *
	 * @access public
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @access public
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'view', 'viewTitle'),
				'users' => array('*'),
			),
			array('allow',
				'actions' => array('create', 'update', 'viewAdmin'),
				'roles' => array(User::LEVEL_STAFF),
			),
			array('allow',
				'actions'=>array('admin', 'delete'),
				'roles' => array(User::LEVEL_BOARD),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular project by ID.
	 *
	 * @param integer $id the ID of the model to be displayed
	 *
	 * @access public
	 * @return void
	 */
	public function actionView($id)
	{
		$model = $this->_loadModel($id);
		if (is_null($model))
			throw new CHttpException(404);
		
		$this->layout = '//layouts/column1';
		
		$this->render(
			'view',
			array(
				'model' => $model,
			)
		);
	}
	
	/**
	 * Displays a particular project by title.
	 *
	 * @param integer $id the ID of the model to be displayed
	 *
	 * @access public
	 * @return void
	 */
	public function actionViewTitle($title)
	{
		$this->layout = '//layouts/column1';
		
		$model = Project::model()->findByAttributes(array(
			'slug' => $title,
		));
		
		if (is_null($model))
			throw new CHttpException(404);
		
		$this->actionView($model->id);
	}
	
	/**
	 * Displays a particular project by ID.
	 *
	 * @param integer $id the ID of the model to be displayed
	 *
	 * @access public
	 * @return void
	 */
	public function actionViewAdmin($id)
	{
		$model = $this->_loadModel($id, array('images'));

		$dates = new ProjectDate;
		$dates->project_id = $model->id;

		$staff = new ProjectStaff;
		$staff->project_id = $model->id;
		
		//Reorder images
		if (isset($_POST['image_id'])) {
			$order = array();
			
			foreach ($_POST['image_id'] as $key => $imageId) {
				$imageId = @intval($imageId);
				$orderId = @intval($_POST['order_id'][$key]);
				
				$order[$imageId] = $orderId;
			}
			
			foreach ($images as $image) {
				$change = false;
				if (isset($order[$image->id]) and ($order[$image->id] != $image->order_id)) {
					$change = true;
					$image->order_id = $order[$image->id];
					$image->save();
				}
				
				if ($change) {
					$project = Project::model()->findByPk($id);
					$images = $project->images;
					Yii::app()->user->setFlash('success', Yii::t('Project.Image', 'Reihenfolge der Bilder geändert.'));
				}
			}
			
			if (Yii::app()->request->isAjaxRequest) {
				Yii::app()->end();
			}
		}

		$this->render(
			'viewAdmin',
			array(
				'model' => $model,
				'dates' => $dates,
				'staff' => $staff,
				'images' => $model->images,
			)
		);
	}

	/**
	 * Creates a new project.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @access public
	 * @return void
	 */
	public function actionCreate()
	{
		$model = new Project;
		$model->user_id = Yii::app()->user->id;

		if (isset($_POST['Project'])) {
			$model->attributes = $_POST['Project'];
			if ($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render(
			'create',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Updates a particular project.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id the ID of the model to be updated
	 *
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$model = $this->_loadModel($id);

		if (isset($_POST['Project'])) {
			$model->attributes = $_POST['Project'];

			if ($model->save())
				$this->redirect(array('viewAdmin', 'id' => $model->id));
		}

		$this->render(
			'update',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Deletes a particular project.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param integer $id the ID of the model to be deleted
	 *
	 * @access public
	 * @return void
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all projects.
	 *
	 * @access public
	 * @return void
	 */
	public function actionIndex($year = null, $month = null, $day = null)
	{
		$this->layout = '//layouts/column1';
		
		//List projects where the end of the project is in the future
		if (!is_null($year) and !is_null($month) and !is_null($day)) {
			// [int hour [, int minute [, int second [, int month [, int day [, int year
			$year = intval($year);
			$month = intval($month);
			$day = intval($day);
			
			$date = mktime(0, 0, 0, $month, $day, $year);
			
			$where = "
				p.id = d.project_id AND
				DATE(d.date_start) <= '" . date('Y-m-d', $date) . "' AND DATE(d.date_end) >= '" . date('Y-m-d', $date) . "'
			";
			
			$curDate = Yii::t('Project', 'am {date}', array('{date}' => date('d.m.Y', $date)));
		}
		elseif (!is_null($year) and !is_null($month)) {
			$year = intval($year);
			$month = intval($month);
			
			$date_start = mktime(0, 0, 0, $month, 1, $year);
			$date_end = mktime(23, 59, 59, $month+1, 0, $year);
			
			$where = "
				p.id = d.project_id AND
				DATE(d.date_start) <= '" . date('Y-m-d', $date_start) . "' AND DATE(d.date_end) >= '" . date('Y-m-d', $date_end) . "'
			";
			
			$curDate = Yii::t('Project', 'im {date}', array('{date}' => strftime('%B %G', $date_start)));
		}
		else {
			$where = "
				p.id = d.project_id AND 
				d.date_end >= NOW()
			";
			
			$curDate = '';
		}
		
		$sql = "
			SELECT DISTINCT 
				p.* 
			FROM 
				{{project}} as p, 
				{{project_date}} as d 
			WHERE 
				" . $where . "
			ORDER BY 
				d.date_end ASC;";
		
		$projects = Project::model()->index()->findAllBySql($sql);
		
		$this->render('index',array(
			'projects' => $projects,
			'curDate' => $curDate,
		));
	}

	/**
	 * Manages all projects.
	 *
	 * @access public
	 * @return void
	 */
	public function actionAdmin()
	{
		$model = new Project('search');
		
		$model->unsetAttributes();
		
		if (isset($_GET['Project']))
			$model->attributes = $_GET['Project'];

		$this->render(
			'admin',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param integer the ID of the model to be loaded
	 *
	 * @access private
	 * @return Project
	 */
	private function _loadModel($id, $with = array())
	{
		$model = Project::model()->with($with)->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404, Yii::t('Project', 'Das Projekt existiert leider nicht!'));

		return $model;
	}
}
