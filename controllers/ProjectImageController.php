<?php

/**
 * Project image controller.
 *
 * @package Project
 * @subpackage Image
 */
class ProjectImageController extends Controller
{
	/**
	 * Default layout.
	 *
	 * @access public
	 * @var string
	 */
	public $layout = '//layouts/admin';

	/**
	 * Controller filters.
	 *
	 * @access public
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @access public
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('create', 'update', 'delete', 'resize', 'order'),
				'roles' => array(User::LEVEL_BOARD),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * Upload a new image.
	 *
	 * @param integer $id Project ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionCreate($id)
	{
		$project = Project::model()->findByPk($id);
		
		if (is_null($project))
			throw new CHttpException(404);
		
		$model=new ProjectImage;
		$model->project_id = $project->id;

		if (isset($_POST['ProjectImage'])) {
			$model->attributes = $_POST['ProjectImage'];
			$model->image = CUploadedFile::getInstance($model,'image');
			
			if (!is_null($model->image)) {
				$model->filename = md5_file($model->image->tempName);
				$model->extension = $model->image->extensionName;
				$model->mime = $model->image->type;
			}
			
			$models = ProjectImage::model()->findAllByAttributes(
				array(
					'project_id' => $project->id,
				)
			);
			
			$max = 0;
			foreach ($models as $modelOrder) {
				if ($modelOrder->order_id >= $max)
					$max = $modelOrder->order_id + 1;
			}
			$model->order_id = $max;
			
			if ($model->save()) {
				$path = Yii::app()->basePath.'/../images/projects/'.$project->id.'/';
				if (!is_dir($path))
					mkdir($path, 0755);
				
				if (!file_exists($path.$model->filename)) {
					if ($model->image->saveAs($path.$model->filename)) {
						$this->redirect(array('resize', 'id' => $model->id, 'add' => true));
					}
					else {
						Yii::app()->user->setFlash('error', Yii::t('Project.Image', 'Das Bild konnte nicht auf dem Server gespeichert werden!'));
					}
				}
				else {
					$button = EBootstrap::tag('p', array(), EBootstrap::ibutton(Yii::t('Project.Image', 'Endgültig löschen'), $this->createUrl('/project/projectImage/deleteFile', array('id' => $project->id, 'hash' => $model->filename)), 'danger'));
					Yii::app()->user->setFlash('error-block', '<p>'.Yii::t('Project.Image', 'Das Bild wurde schon einmal für das Projekt hochgeladen! Möchtest du das alte Bild löschen?'.'</p>'.$button));
				}
			}
		}

		$this->render(
			'create',
			array(
				'model' => $model,
				'project' => $project,
			)
		);
	}
	
	/**
	 * Resize and crop image.
	 *
	 * @param integer $id ProjectImage ID
	 * @param boolean $add Whether the image is recently added (default: false)
	 *
	 * @access public
	 * @return void 
	 */
	public function actionResize($id, $add = false) {
		$model = ProjectImage::model()->with('theProject')->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);
		
		$path = Yii::app()->basePath.'/../images/projects/'.$model->theProject->id.'/';
		$original = Yii::app()->request->baseUrl.'/images/projects/'.$model->theProject->id.'/'.$model->filename;
		
		list($width, $height) = getimagesize($path . $model->filename);
		
		if (isset($_POST['resize']) or ($add)) {
			$resizeBase = new ResizeImage;
			$error = 0;
			$max = 0;
			
			//Delete old images
			$sql = 'DELETE FROM {{project_image_option}} WHERE `image_id` = :image_id';
			$delete = Yii::app()->db->createCommand($sql);
			$delete->bindValue(':image_id', $model->id);
			$result = $delete->execute();
			
			//Resize image in each format
			foreach ($model->formats as $format) {
				$max++;
				$dst = $path . $model->filename . '_' . $format['title'];
				
				//Get image size
				if (!$add) {
					$x1 = round($_POST['projectimagesize'][$format['id']]['x1']);
					$y1 = round($_POST['projectimagesize'][$format['id']]['y1']);
					$x2 = round($_POST['projectimagesize'][$format['id']]['x2']);
					$y2 = round($_POST['projectimagesize'][$format['id']]['y2']);
					
					if ($x2 == 0 or $y2 == 0) {
						$x2 = $x1 + $format['width'];
						$y2 = $y1 + $format['height'];
					}
				} else {
					//Default values
					$x1 = null;
					$y1 = null;
					$x2 = null;
					$y2 = null;
				}
				
				//Resize image
				$resize = $resizeBase->resize($path.$model->filename, $dst, $model->extension, $format['width'], $format['height'], $x1, $y1, $x2, $y2, $format['crop']);
				if ($resize !== true) {
					switch ($resize) {
						case $resizeBase::ERROR_WRONG_FORMAT: $error = "Wrong input format"; break;
						case $resizeBase::ERROR_TOO_SMALL: $error = "Image dimensions are too small"; break;
						case $resizeBase::ERROR_SAVE: $error = "Can not save image"; break;
						case $resizeBase::ERROR_FILE_EXISTS: $error = "New image file does not exist"; break;
						default: $error = "Unknown error";
					}
					
					Yii::log('Could not resize image `'.$path.$model->filename.'` to `'.$dst.'`: '.$error, 'warning', 'application.modules.project.controllers.ProjectImageController');
					$error++;
				}
				elseif (!$add) {
					$sql = "INSERT INTO {{project_image_option}}(image_id, format_id, x1, y1, x2, y2) VALUES (:image_id, :format_id, :x1, :y1, :x2, :y2);";
					$insert = Yii::app()->db->createCommand($sql);
					$insert->bindValue(':image_id', $model->id);
					$insert->bindValue(':format_id', $format['id']);
					$insert->bindValue(':x1', $x1);
					$insert->bindValue(':y1', $y1);
					$insert->bindValue(':x2', $x2);
					$insert->bindValue(':y2', $y2);
					
					$result = $insert->execute();
				}
			}
			
			if ($error === 0) {
				if ($add)
					Yii::app()->user->setFlash('success', Yii::t('Project.Image', 'Bild zu dem Projekt hinzugefügt.', array('{project}' => $model->theProject->title)));
				else
					Yii::app()->user->setFlash('success', Yii::t('Project.Image', 'Der Bildausschnitt wurde geändert.', array('{project}' => $model->theProject->title)));
			} else {
				if ($add)
					Yii::app()->user->setFlash('warning', Yii::t('Project.Image', 'Bild zu dem Projekt `{project}` hinzugefügt. {error} von {max} Vorschaubildern konnten aber nicht erstellt werden!', array('{project}' => $model->theProject->title, '{error}' => $error, '{max}' => $max)));
				else
					Yii::app()->user->setFlash('warning', Yii::t('Project.Image', 'Die Bildausschnitte konnten teilweise nicht geändert werden! {error} von {max} Vorschaubildern wurden nicht erstellt!', array('{project}' => $model->theProject->title, '{error}' => $error, '{max}' => $max)));
			}
			
			$this->redirect(array('/project/project/viewAdmin', 'id' => $model->theProject->id));
		}
		
		$sql = "SELECT * FROM {{project_image_option}} WHERE image_id = :image_id";
		$select = Yii::app()->db->createCommand($sql);
		$select->bindValue(':image_id', $model->id);
		$default = $select->queryAll();
		
		$this->render(
			'resize', 
			array(
				'project' => $model->theProject,
				'model' => $model,
				'image' => $original,
				'width' => $width,
				'height' => $height,
				'default' => $default,
			)
		);
	}
	
	/**
	 * Updates a image.
	 *
	 * @param integer $id ProjectImage ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$model = ProjectImage::model()->with('theProject')->findByPk($id);
		$project = $model->theProject;
		
		if (is_null($model))
			throw new CHttpException(404);
		
		if (!isset($project) or empty($project))
			throw new CHttpException(500);

		if (isset($_POST['ProjectImage'])) {
			$model->attributes = $_POST['ProjectImage'];

			if($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('Project.Image', 'Das Bild wurde bearbeitet.', array('{image}' => $model->title)));
				$this->redirect(array('/project/project/viewAdmin', 'id' => $project->id));
			}
		}

		$this->render(
			'update',
			array(
				'model' => $model,
				'project' => $project,
			)
		);
	}

	/**
	 * Reorders the images in a project.
	 *
	 * @param integer $id Project ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionOrder($id)
	{
		if (isset($_POST['image_id'])) {
			$project = Project::model()->with('images')->findByPk($id);
		
			if (is_null($project))
				throw new CHttpException(404);
			
			$images = $project->images;

			$order = array();
			
			foreach ($_POST['image_id'] as $key => $imageId) {
				$imageId = @intval($imageId);
				$orderId = @intval($_POST['order_id'][$key]);
				
				$order[$imageId] = $orderId;
			}
			
			foreach ($images as $image) {
				$change = false;
				if (isset($order[$image->id]) and ($order[$image->id] != $image->order_id)) {
					$change = true;
					$image->order_id = $order[$image->id];
					$image->save();
				}
				
				if ($change) {
					$project = Project::model()->findByPk($id);
					$images = $project->images;
				}
			}
			
			if (Yii::app()->request->isAjaxRequest) {
				Yii::app()->end();
			}
		} else {
			throw new CHttpException(400);
		}
	}
	
	/**
	 * Deletes an image from a project.
	 *
	 * @param integer $id ProjectImage ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionDelete($id) {
		$model = ProjectImage::model()->with('theProject')->findByPk($id);
		$project = $model->theProject;
		
		if (is_null($model))
			throw new CHttpException(404);
		
		$model->delete();
		
		Yii::app()->user->setFlash('info', Yii::t('Project.Image', 'Das Bild wurde gelöscht.'));
		$this->redirect(array('/project/project/viewAdmin', 'id' => $project->id));
	}
}
