<?php

/**
 * Project Wiki controller
 *
 * @package Project
 * @subpackage Wiki
 */
class ProjectWikiController extends Controller
{
	/**
	 * Default layout.
	 *
	 * @access public
	 * @var string
	 */
	public $layout='//layouts/admin';
	
	/**
	 * Default action
	 *
	 * @access public
	 * @var string
	 */
	public $defaultAction = 'view';

	/**
	 * Get the controller filters
	 *
	 * @access public
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @access public
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('create', 'view'),
				'roles' => array(User::LEVEL_STAFF),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Display wiki.
	 *
	 * @param integer $id Wiki ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionView($id) {
		$model = ProjectWiki::model()->with('pages')->findByPk($id);
		
		if (is_null($model))
			$this->redirect(array('/project/projectWiki/create', 'id' => $id));
		
		if (is_array($model->pages) and isset($model->pages[0]->id))
			$this->redirect(array('/project/projectWikiPage/view', 'id' => $model->pages[0]->id));
		
		$this->render(
			'/projectWiki/view', 
			array(
				'model' => $model,
			)
		);
	}
	
	/**
	 * Create a new wiki for a project.
	 *
	 * @param integer $id Project ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionCreate($id) {
		$project = Project::model()->findByPk($id);
		
		if (is_null($project))
			throw new CHttpException(404, Yii::t('Project.Wiki', 'Das Projekt konnte nicht gefunden werden!'));
		
		//Check if wiki exists for the project
		$wiki = ProjectWiki::model()->findByAttributes(
			array(
				'project_id' => $project->id,
			)
		);
		
		if (is_null($wiki)) {
			//Create new wiki
			$wiki = new ProjectWiki;
			$wiki->project_id = $project->id;
			if ($wiki->save()) {
				Yii::app()->user->setFlash('success', Yii::t('Project.Wiki', 'Wiki für das Projekt `{project}` erstellt.', array('{project}' => $project->title)));
			}
			else {
				throw new CHttpException(500, Yii::t('Project.Wiki', 'Das Wiki konnte nicht erstellt werden!'));
			}
		}

		$this->redirect(array('/project/projectWiki/view', 'id' => $wiki->id));
	}
}
