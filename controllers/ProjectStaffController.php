<?php

/**
 * Project staff controller.
 *
 * @package Project
 * @subpackage Staff
 */
class ProjectStaffController extends Controller
{
	/**
	 * Default layout.
	 *
	 * @access public
	 * @var string
	 */
	public $layout='//layouts/admin';

	/**
	 * Controller filters.
	 *
	 * @access public
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @access public
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('create', 'delete'),
				'roles' => array(User::LEVEL_BOARD),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Creates a new staff member for a project.
	 *
	 * @param integer $id Project ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionCreate($id)
	{
		$project = Project::model()->findByPk($id);
		
		if (is_null($project))
			throw new CHttpException(404);
		
		$model = new ProjectStaff;
		$model->project_id = $project->id;

		if (isset($_POST['ProjectStaff'])) {
			$model->attributes = $_POST['ProjectStaff'];
			
			//Set user either to an existing user or to a username
			if (is_numeric($_POST['ProjectStaff']['user']) and ($_POST['ProjectStaff']['user'] > 0))
				$model->user_id = $_POST['ProjectStaff']['user'];
			else
				$model->user_name = $_POST['ProjectStaff']['user'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('Project.Staff', 'Der Mitarbeiter wurde zu dem Projekt hinzugefügt.'));
				$this->redirect(array('/project/project/viewAdmin', 'id' => $project->id));
			}
		}
		
		//Get users which do not already participate
		$criteria = new CDbCriteria;
		$staff = $project->staff;
		$notIn = array();
		foreach ($staff as $member) {
			$notIn[] = $member->user_id;
		}
		$criteria->addNotInCondition('id', $notIn);
		$users = User::model()->findAll($criteria);

		$this->render(
			'create',
			array(
				'model' => $model,
				'project' => $project,
				'users' => $users,
			)
		);
	}
	
	/**
	 * Delete a staff member from a project.
	 *
	 * @param integer $id ProjectStaff ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionDelete($id) {
		$model = ProjectStaff::model()->findByPk($id);
		$project = $model->theProject;
		
		if (is_null($model))
			throw new CHttpException(404);
		
		$model->delete();
		
		Yii::app()->user->setFlash('info', Yii::t('Project.Staff', 'Der Benutzer wurde von dem Projekt entfernt.'));
		$this->redirect(array('/project/project/viewAdmin', 'id' => $project->id));
	}
}
