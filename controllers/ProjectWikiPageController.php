<?php

/**
 * Project wiki page controller.
 *
 * @package Project
 * @subpackage Wiki
 */
class ProjectWikiPageController extends Controller
{
	/**
	 * Default page layout.
	 *
	 * @access public
	 * @var string
	 */
	public $layout='//layouts/admin';
	
	public $defaultAction = 'view';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @access public
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('view', 'create', 'update', 'delete'),
				'roles' => array(User::LEVEL_STAFF),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Display wiki page.
	 *
	 * @param integer $id ProjectWikiPage ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionView($id) {
		$model = ProjectWikiPage::model()->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);
		
		$this->render(
			'/projectWikiPage/view', 
			array(
				'model' => $model,
			)
		);
	}
	
	/**
	 * Create new wiki page.
	 *
	 * @param integer $id ProjectWiki ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionCreate($id) {
		$wiki = ProjectWiki::model()->findByPk($id);
		
		if (is_null($wiki))
			throw new CHttpException(404);
		
		$model = new ProjectWikiPage;
		$model->wiki_id = $wiki->id;
		
		if (isset($_POST['ProjectWikiPage'])) {
			$model->attributes = $_POST['ProjectWikiPage'];
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('Project.Wiki', '<strong>Die Seite wurde gespeichert.</strong>'));
				$this->redirect(array('/project/projectWikiPage/view', 'id' => $model->id));
			}
		}

		$this->render(
			'create', 
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Update a wiki page.
	 *
	 * @param integer $id Wiki page ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$model = ProjectWikiPage::model()->findByPk($id);

		if (is_null($model))
			throw new CHttpException(404, Yii::t('Project.Wiki', 'Die Seite konnte leider nicht gefunden werden!'));

		if (isset($_POST['ProjectWikiPage'])) {
			$model->attributes = $_POST['ProjectWikiPage'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('Project.Wiki', '<strong>Die Seite wurde bearbeitet.</strong>'));
				$this->redirect(array('/project/projectWikiPage/view', 'id' => $model->id));
			}
		}

		$this->render(
			'update',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Delete wiki page.
	 *
	 * @param integer $id ProjectWikiPage ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionDelete($id)
	{
		$model = ProjectWikiPage::model()->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);

		$model->delete();

		$this->redirect(array('/project/projectWiki/view', 'id' => $model->theWiki->id));
	}
}
