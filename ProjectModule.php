<?php

class ProjectModule extends CWebModule
{
	/**
	 * Init application.
	 *
	 * @access protected
	 * @return void
	 */
	protected function init()
	{
		Yii::import('application.modules.project.models.*');
		Yii::import('application.modules.project.portlets.*');

		return parent::init();
	}
}