<?php

Yii::import('zii.widgets.CPortlet');

class Projects extends CPortlet {
	public $title = 'Projekte';
	public $count = 10;
	
	protected function renderContent() {
		$models = Project::model()->index()->findAll();

		echo EBootstrap::openTag('ul', array('class' => 'unstyled'));
		
		foreach ($models as $model) {
			if (count($model->dates) > 0)
				echo EBootstrap::tag('li', array(), EBootstrap::link($model->title, array('/project/project/viewTitle', 'title' => $model->slug)));
		}
		
		echo EBootstrap::closeTag('ul');
	}
}

?>