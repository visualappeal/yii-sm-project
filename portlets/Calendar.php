<?php

Yii::import('zii.widgets.CPortlet');

class Calendar extends CPortlet {
	public $month;
	public $year;
	public $project = null;
	
	public $title;
	
	public $dayNames = array('Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So');
	
	public function init() {
		$this->title = Yii::t('Site', 'Kalender');
	
		parent::init();
	}
	
	public function listProjects($timestamp, $models) {
		$projects = array();
		
		foreach ($models as $model) {
			$dateStartTs = strtotime($model->date_start);
			$dateStart = mktime(0, 0, 0, date('n', $dateStartTs), date('j', $dateStartTs), date('Y', $dateStartTs));
			$dateEndTs = strtotime($model->date_end);
			$dateEnd = mktime(0, 0, 0, date('n', $dateEndTs), date('j', $dateEndTs), date('Y', $dateEndTs));
			if ($timestamp >= $dateStart && $timestamp <= $dateEnd) {
				$projects[] = $model->theProject;
			}
		}

		return $projects;
	}
	
	public function hasProject($projects) {
		if (!is_null($this->project)) {
			foreach ($projects as $project) {
				if ($project->id == $this->project)
					return true;
			}
		}
		
		return false;
	}
	
	protected function renderContent() {		
		$dateStart = mktime(0, 0, 0, $this->month, 1, $this->year);
		
		$nextMonth = $this->month + 1;
		$nextYear = $this->year;
		if ($nextMonth > 12) {
			$nextMonth = 1;
			$nextYear++;
		}
		$dateEnd = mktime(0, 0, 0, $nextMonth, 0, $nextYear);
		
		$days = array();
		
		echo EBootstrap::openTag('div', array('id' => 'project-calendar'));
		echo EBootstrap::openTag('table');
		
		//Days of month
		$today = date('d');
		for ($i = 1; $i <= date('t', $dateStart); $i++) {
			$date = mktime(0, 0, 0, $this->month, $i, $this->year);
			
			if ($today == $i) {
				$days[$date] = 'day day-today';
			}
			else {
				$days[$date] = 'day';
			}
		}
		
		//Fill beginning with dates of the previous month
		$weekStart = date('w', $dateStart);
		$weekPrev = 0;
		for ($i = $weekStart; $i > 1; $i--) {
			$prevDate = mktime(0, 0, 0, $this->month, $weekPrev, $this->year);
			$days[$prevDate] = 'day day-prev';
			
			$weekPrev--;
		}
		
		//Fill end with dates of the next month
		$weekNext = date('w', $dateEnd);
		$i = date('t', $dateStart)+1;
		while ($weekNext != 0) {
			$nextDate = mktime(0, 0, 0, $this->month, $i, $this->year);
			$days[$nextDate] = 'day day-next';	
			
			$weekNext = date('w', $nextDate);
			$i++;
		}
		
		ksort($days);
		
		//Get dates
		$daysNum = array_keys($days);
		$criteria = new CDbCriteria;
		$criteria->addCondition('t.date_start >= :date_start');
		$criteria->addCondition('t.date_start <= :date_end');
		$criteria->params = array(
			':date_start' => date('Y-m-d', $daysNum[0]),
			':date_end' => date('Y-m-d', $daysNum[34]),
		);
		
		$models = ProjectDate::model()->with('theProject')->findAll($criteria);
		
		//Display calendar
		echo EBootstrap::openTag('thead');
		
		echo EBootstrap::openTag('tr');
		foreach ($this->dayNames as $dayName) {
			echo EBootstrap::tag('th', array(), $dayName);
		}
		echo EBootstrap::closeTag('tr');
		echo EBootstrap::closeTag('thead');
		
		echo EBootstrap::openTag('tbody');
		$i = 0;
		foreach ($days as $timestamp => $classes) {
			if ($i == 0) {
				echo EBootstrap::openTag('tr');
			}
			elseif ($i % 7 == 0) {
				echo EBootstrap::closeTag('tr');
				echo EBootstrap::openTag('tr');
			}
			
			$projects = $this->listProjects($timestamp, $models);
			
			if ($this->hasProject($projects)) {
				$classes .= ' day-project';
			}
			
			echo EBootstrap::openTag('td', array('class' => $classes));
			
			if (count($projects)) {
				echo EBootstrap::openTag('a', array('href' => Yii::app()->createUrl('/project/project/index', array('year' => date('Y', $timestamp), 'month' => date('m', $timestamp), 'day' => date('d', $timestamp)))));
			}
			
			echo date('d', $timestamp);
			
			if (count($projects)) {
				echo EBootstrap::closeTag('a');
			}
			
			echo EBootstrap::closeTag('td');
			
			if ($i == 34) {
				echo EBootstrap::closeTag('tr');
			}
			
			$i++;
		}
		echo EBootstrap::closeTag('tbody');
		
		echo EBootstrap::closeTag('table');
		echo EBootstrap::closeTag('div');
	}
	
}

?>