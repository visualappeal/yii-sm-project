<?php

Yii::import('zii.widgets.CPortlet');

class DashboardProjects extends DashboardPortlet {
	public function init() {
		$this->title = Yii::t('Site', 'Kalender');
	
		parent::init();
	}
	
	protected function renderContent() {
		$projects = Project::model()->dashboard()->findAll();
		
		if (count($projects)) {
			echo EBootstrap::openTag('ul');
			
			foreach ($projects as $project) {
				$title = $project->title;
				if (count($project->dates)) {
					$title .= ' (' . date('d.m H:i', strtotime($project->dates[count($project->dates)-1]->date_start)) . ')';
				}
				echo EBootstrap::tag('li', array(), EBootstrap::link($title, array('/project/project/viewAdmin', 'id' => $project->id)));
			}
			
			echo EBootstrap::closeTag('ul');
		}
		else {
			echo EBootstrap::tag('em', array(), Yii::t('Project', 'Du hast im Moment keine Projekte und nimmst auch an keinen Teil.'));
		}
	}
}

?>